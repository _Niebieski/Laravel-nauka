<?php

interface ProbaInterface
{
    public function setAuto();
}

class Auto implements ProbaInterface
{
    public function setAuto() {
        $this->auto = 'proba';

        return $this;
    }

    public function setBra() {
        $this->bra = 'bra';

        return $this;
    }
}

class Auto2
{
    protected $dziala = 1;
}

class Samochod
{
    protected $model;
    public $bungo;

    public function __construct(ProbaInterface $proba)
    {
        $this->auto = $proba->setBra()->bra;
    }

    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    public function getBungo()
    {
        if ($this->model == 'dsdsd') {
            $this->bungo = $this->auto;
        }
    }
}

$samochod = new Samochod(new Auto);
$samochod->setModel('dsdsd')->getBungo();

echo $samochod->bungo;
//wyświetli Astra
