<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class LinkedSocialAccount extends Model
{
    protected $fillable = ['provider_name', 'provider_id' ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
